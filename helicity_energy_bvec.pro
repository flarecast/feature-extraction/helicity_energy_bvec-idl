FUNCTION helicity_energy_bvec, br_t0, bt_t0, bp_t0, idx_t0, br_t1, bt_t1, bp_t1, idx_t1, binxy_helicity_energy=binxy_helicity_energy, set_mpil=set_mpil, binxy_mpil=binxy_mpil, $
                               extend=extend, fwhm_align=fwhm_align, fwhm_dave4vm=fwhm_dave4vm, mpil_info=mpil_info, mpil_xy=mpil_xy

;+
; Name: helicity_energy_bvec.pro
;
; Purpose: calculates magnetic helicity and energy injection rates through the photosphere of active regions.
;
; Calling Sequence:
;   output_helicity_energy_bvec=helicity_energy_bvec(br_t0,bt_t0,bp_t0,idx_t0,br_t1,bt_t1,bp_t1,idx_t1[,binxy_helicity_energy=binxy_helicity_energy][,set_mpil=set_mpil][,binxy_mpil=binxy_mpil], $
;                          [extend=extend][,fwhm_align=fwhm_align][,fwhm_dave4vm=fwhm_dave4vm][,mpil_info=mpil_info][,mpil_xy=mpil_xy])
;
; Input parameters:
;   br_t0 - hmi_sharp_cea_720s Br image at t0 (note that t1 > t0)
;   bt_t0 - hmi_sharp_cea_720s Bt image at t0
;   bp_t0 - hmi_sharp_cea_720s Bp image at t0
;   idx_t0 - hmi_sharp_cea_720s meta data at t0
;   br_t1 - hmi_sharp_cea_720s Br image at t1
;   bt_t1 - hmi_sharp_cea_720s Bt image at t1
;   bp_t1 - hmi_sharp_cea_720s Bp image at t1
;   idx_t1 - hmi_sharp_cea_720s meta data at t1
;    
; Optional Input Keyword Parameters:
;   binxy_helicity_energy - number for xy spatial binning of the sharp magnetic field input data related to calculation of magnetic helicity and energy injection rates [in pixels] ; default=1 (i.e., no binning)
;   set_mpil - if set, calculate magnetic helicity and energy injection rates only near magnetic polarity inversion lines (MPILs) ; default=0
;   binxy_mpil - number for xy spatial binning of sharp magnetic field input data related to MPIL detection [pixels] ; default=1 
;   extend - length of a square box surrounding each of MPIL pixels [Mm]. The box region will be considered for calculating magnetic helicity and energy injection rates therein; default=5
;   fwhm_align - full width at half maximum for sharp image alignment [arcsecs] ; default=10
;   fwhm_dave4vm - full width at half maximum for flow field calculation [arcsecs] ; default=4
;   mpil_info - information on types of MPILs
;   mpil_xy - x and y coordinates of MPILs [pixels]
;
; Output Parameters:
;   output_helicity_energy - a structure having 22 tags related to helicity and energy injection properties
;                 pos_dhdt_in: postive dhdt_in [10^35 Mx^2/sec]
;                 abs_neg_dhdt_in: absolute of negative dhdt_in [10^35 Mx^2/sec]
;                 abs_tot_dhdt_in: absolute of total dhdt_in [10^35 Mx^2/sec]
;                 tot_uns_dhdt_in: total unsigned dhdt_in [10^35 Mx^2/sec]
;                 pos_dhdt_sh: postive dhdt_sh [10^35 Mx^2/sec]
;                 abs_neg_dhdt_sh: absolute of negative dhdt_sh [10^35 Mx^2/sec]
;                 abs_tot_dhdt_sh: absolute of total dhdt_sh [10^35 Mx^2/sec]
;                 tot_uns_dhdt_sh: total unsigned dhdt_sh [10^35 Mx^2/sec]
;                 abs_tot_dhdt: absolute of total dhdt [10^35 Mx^2/sec]
;                 abs_tot_dhdt_in_plus_sh: abs_tot_dhdt_in + abs_tot_dhdt_sh [10^35 Mx^2/sec]
;                 tot_uns_dhdt: total unsigned dhdt [10^35 Mx^2/sec]                
;                 pos_dedt_in: postive dedt_in [10^24 erg/sec]
;                 abs_neg_dedt_in: absolute of negative dedt_in [10^24 erg/sec]
;                 abs_tot_dedt_in: absolute of total dedt_in [10^24 erg/sec]
;                 tot_uns_dedt_in: total unsigned dedt_in [10^24 erg/sec]         
;                 pos_dedt_sh: postive dedt_sh [10^24 erg/sec]
;                 abs_neg_dedt_sh: absolute of negative dedt_sh [10^24 erg/sec]
;                 abs_tot_dedt_sh: absolute of total dedt_sh [10^24 erg/sec]
;                 tot_uns_dedt_sh: total unsigned dedt_sh [10^24 erg/sec]
;                 abs_tot_dedt: absolute of total dedt [10^24 erg/sec]
;                 abs_tot_dhdt_in_plus_sh: abs_tot_dedt_in + abs_tot_dedt_sh [10^24 erg/sec]
;                 tot_uns_dedt: total unsigned dedt [10^24 erg/sec]
;
; Modificatioin History:
;           11-Aug-2016 - Sung-Hong Park - written
;-

IF NOT ISVALID(binxy_helicity_energy) THEN binxy_helicity_energy=1
IF NOT ISVALID(set_mpil) THEN set_mpil=0
IF NOT ISVALID(binxy_mpil) THEN binxy_mpil=1
IF NOT ISVALID(extend) THEN extend=5. ; Mm
IF NOT ISVALID(fwhm_align) THEN fwhm_align=10. ; arcsecs
IF NOT ISVALID(fwhm_dave4vm) THEN fwhm_dave4vm=4d ; arcsecs

extendpix=FIX(extend/0.36442476) ; pixels
delmm=0.36442476*binxy_helicity_energy

IF ((N_ELEMENTS(br_t0[*,0])/binxy_mpil GT 8) AND (N_ELEMENTS(br_t0[0,*])/binxy_mpil GT 8) AND (N_ELEMENTS(br_t1[*,0])/binxy_mpil GT 8) AND (N_ELEMENTS(br_t1[0,*])/binxy_mpil GT 8)) THEN BEGIN

  IF (FIX(set_mpil) EQ 0) THEN BEGIN
    
    t_obs_date0=anytim(round(idx_t0.t_obs-6.3072000E+7,/L64),/CCSDS)+'Z'
    s0=SIZE(br_t0)
    br_t0=CONGRID(br_t0, s0[1]/binxy_helicity_energy*binxy_helicity_energy, s0[2]/binxy_helicity_energy*binxy_helicity_energy) ;bz
    br_t0=REBIN(br_t0, s0[1]/binxy_helicity_energy, s0[2]/binxy_helicity_energy)
  
    bt_t0=CONGRID(-1.*bt_t0, s0[1]/binxy_helicity_energy*binxy_helicity_energy, s0[2]/binxy_helicity_energy*binxy_helicity_energy) ;by
    bt_t0=REBIN(bt_t0, s0[1]/binxy_helicity_energy, s0[2]/binxy_helicity_energy)
  
    bp_t0=CONGRID(bp_t0, s0[1]/binxy_helicity_energy*binxy_helicity_energy, s0[2]/binxy_helicity_energy*binxy_helicity_energy) ;bx
    bp_t0=REBIN(bp_t0, s0[1]/binxy_helicity_energy, s0[2]/binxy_helicity_energy)
  
    t_obs_date1=anytim(round(idx_t1.t_obs-6.3072000E+7,/L64),/CCSDS)+'Z'
    s1=SIZE(br_t1)
    br_t1=CONGRID(br_t1, s1[1]/binxy_helicity_energy*binxy_helicity_energy, s1[2]/binxy_helicity_energy*binxy_helicity_energy)
    br_t1=REBIN(br_t1, s1[1]/binxy_helicity_energy, s1[2]/binxy_helicity_energy)
  
    bt_t1=CONGRID(-1.*bt_t1, s1[1]/binxy_helicity_energy*binxy_helicity_energy, s1[2]/binxy_helicity_energy*binxy_helicity_energy)
    bt_t1=REBIN(bt_t1, s1[1]/binxy_helicity_energy, s1[2]/binxy_helicity_energy)
  
    bp_t1=CONGRID(bp_t1, s1[1]/binxy_helicity_energy*binxy_helicity_energy, s1[2]/binxy_helicity_energy*binxy_helicity_energy)
    bp_t1=REBIN(bp_t1, s1[1]/binxy_helicity_energy, s1[2]/binxy_helicity_energy)
  
    ; checking whether the two images have a common field-of-view or not
    IF (idx_t0.londtmax LT idx_t1.londtmin) THEN BEGIN
  
      output_helicity_energy={pos_dhdt_in:0., abs_neg_dhdt_in:0., abs_tot_dhdt_in:0., tot_uns_dhdt_in:0., pos_dhdt_sh:0., abs_neg_dhdt_sh:0., abs_tot_dhdt_sh:0., tot_uns_dhdt_sh:0., $
                              abs_tot_dhdt:0., abs_tot_dhdt_in_plus_sh:0., tot_uns_dhdt:0., pos_dedt_in:0., abs_neg_dedt_in:0., abs_tot_dedt_in:0., tot_uns_dedt_in:0., pos_dedt_sh:0., $ 
                              abs_neg_dedt_sh:0., abs_tot_dedt_sh:0., tot_uns_dedt_sh:0., abs_tot_dedt:0., abs_tot_dedt_in_plus_sh:0., tot_uns_dedt:0.}
      
    ENDIF ELSE BEGIN
  
      ; cut the sub-regions from the two images, which have the same x and y sizes
      IF (s0[1] le s1[1]) THEN BEGIN
        IF (s0[2] le s1[2]) THEN BEGIN
          bp_t0=bp_t0[0:s0[1]-1,0:s0[2]-1]
          bp_t1=bp_t1[(s1[1]-s0[1])/2:(s1[1]-1-ROUND((s1[1]-s0[1])/2.)),(s1[2]-s0[2])/2:(s1[2]-1-ROUND((s1[2]-s0[2])/2.))]
          bt_t0=bt_t0[0:s0[1]-1,0:s0[2]-1]
          bt_t1=bt_t1[(s1[1]-s0[1])/2:(s1[1]-1-ROUND((s1[1]-s0[1])/2.)),(s1[2]-s0[2])/2:(s1[2]-1-ROUND((s1[2]-s0[2])/2.))]
          br_t0=br_t0[0:s0[1]-1,0:s0[2]-1]
          br_t1=br_t1[(s1[1]-s0[1])/2:(s1[1]-1-ROUND((s1[1]-s0[1])/2.)),(s1[2]-s0[2])/2:(s1[2]-1-ROUND((s1[2]-s0[2])/2.))]
        ENDIF ELSE BEGIN
          bp_t0=bp_t0[0:s0[1]-1,(s0[2]-s1[2])/2:(s0[2]-1-ROUND((s0[2]-s1[2])/2.))]
          bp_t1=bp_t1[(s1[1]-s0[1])/2:(s1[1]-1-ROUND((s1[1]-s0[1])/2.)),0:s1[2]-1]
          bt_t0=bt_t0[0:s0[1]-1,(s0[2]-s1[2])/2:(s0[2]-1-ROUND((s0[2]-s1[2])/2.))]
          bt_t1=bt_t1[(s1[1]-s0[1])/2:(s1[1]-1-ROUND((s1[1]-s0[1])/2.)),0:s1[2]-1]
          br_t0=br_t0[0:s0[1]-1,(s0[2]-s1[2])/2:(s0[2]-1-ROUND((s0[2]-s1[2])/2.))]
          br_t1=br_t1[(s1[1]-s0[1])/2:(s1[1]-1-ROUND((s1[1]-s0[1])/2.)),0:s1[2]-1]
        ENDELSE
      ENDIF ELSE BEGIN
        IF (s0[2] le s1[2]) THEN BEGIN
          bp_t0=bp_t0[(s0[1]-s1[1])/2:(s0[1]-1-ROUND((s0[1]-s1[1])/2.)),0:s0[2]-1]
          bp_t1=bp_t1[0:s1[1]-1,(s1[2]-s0[2])/2:(s1[2]-1-ROUND((s1[2]-s0[2])/2.))]
          bt_t0=bt_t0[(s0[1]-s1[1])/2:(s0[1]-1-ROUND((s0[1]-s1[1])/2.)),0:s0[2]-1]
          bt_t1=bt_t1[0:s1[1]-1,(s1[2]-s0[2])/2:(s1[2]-1-ROUND((s1[2]-s0[2])/2.))]
          br_t0=br_t0[(s0[1]-s1[1])/2:(s0[1]-1-ROUND((s0[1]-s1[1])/2.)),0:s0[2]-1]
          br_t1=br_t1[0:s1[1]-1,(s1[2]-s0[2])/2:(s1[2]-1-ROUND((s1[2]-s0[2])/2.))]
        ENDIF ELSE BEGIN
          bp_t0=bp_t0[(s0[1]-s1[1])/2:(s0[1]-1-ROUND((s0[1]-s1[1])/2.)),(s0[2]-s1[2])/2:(s0[2]-1-ROUND((s0[2]-s1[2])/2.))]
          bp_t1=bp_t1[0:s1[1]-1,0:s1[2]-1]
          bt_t0=bt_t0[(s0[1]-s1[1])/2:(s0[1]-1-ROUND((s0[1]-s1[1])/2.)),(s0[2]-s1[2])/2:(s0[2]-1-ROUND((s0[2]-s1[2])/2.))]
          bt_t1=bt_t1[0:s1[1]-1,0:s1[2]-1]
          br_t0=br_t0[(s0[1]-s1[1])/2:(s0[1]-1-ROUND((s0[1]-s1[1])/2.)),(s0[2]-s1[2])/2:(s0[2]-1-ROUND((s0[2]-s1[2])/2.))]
          br_t1=br_t1[0:s1[1]-1,0:s1[2]-1]
        ENDELSE
      ENDELSE
  
      ; alignment using a correlation method
      s=SIZE(br_t0)
  
      sht=DAVE_MULTI(fwhm_align/delmm/0.725,  smooth(br_t0,ROUND(5./delmm),/EDGE_TRUNCATE,/NAN), smooth(br_t1,ROUND(5./delmm),/EDGE_TRUNCATE,/NAN))
      xsht=REFORM(sht[0,*,*],s[1],s[2])
      xshtv=MEAN(xsht[WHERE(ABS(br_t0) GT 500.)], /nan)
      ysht=REFORM(sht[1,*,*],s[1],s[2])
      yshtv=MEAN(ysht[WHERE(ABS(br_t0) GT 500.)], /nan)
      br_t1=SHIFT(br_t1,-1*FIX(ROUND(xshtv)),-1*FIX(ROUND(yshtv)))
      bp_t1=SHIFT(bp_t1,-1*FIX(ROUND(xshtv)),-1*FIX(ROUND(yshtv)))
      bt_t1=SHIFT(bt_t1,-1*FIX(ROUND(xshtv)),-1*FIX(ROUND(yshtv)))
  
      ; cut the "co-alinged" sub-regions from the two images
      IF FIX(ROUND(xshtv)) ge 0 THEN BEGIN
        IF FIX(ROUND(yshtv)) ge 0 THEN BEGIN
          bp_t0=bp_t0[0:s[1]-1-FIX(ROUND(xshtv)),0:s[2]-1-FIX(ROUND(yshtv))]
          bp_t1=bp_t1[0:s[1]-1-FIX(ROUND(xshtv)),0:s[2]-1-FIX(ROUND(yshtv))]
          bt_t0=bt_t0[0:s[1]-1-FIX(ROUND(xshtv)),0:s[2]-1-FIX(ROUND(yshtv))]
          bt_t1=bt_t1[0:s[1]-1-FIX(ROUND(xshtv)),0:s[2]-1-FIX(ROUND(yshtv))]
          br_t0=br_t0[0:s[1]-1-FIX(ROUND(xshtv)),0:s[2]-1-FIX(ROUND(yshtv))]
          br_t1=br_t1[0:s[1]-1-FIX(ROUND(xshtv)),0:s[2]-1-FIX(ROUND(yshtv))]
        ENDIF ELSE BEGIN
          bp_t0=bp_t0[0:s[1]-1-FIX(ROUND(xshtv)),(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          bp_t1=bp_t1[0:s[1]-1-FIX(ROUND(xshtv)),(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          bt_t0=bt_t0[0:s[1]-1-FIX(ROUND(xshtv)),(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          bt_t1=bt_t1[0:s[1]-1-FIX(ROUND(xshtv)),(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          br_t0=br_t0[0:s[1]-1-FIX(ROUND(xshtv)),(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          br_t1=br_t1[0:s[1]-1-FIX(ROUND(xshtv)),(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
        ENDELSE
      ENDIF ELSE BEGIN
        IF FIX(ROUND(yshtv)) ge 0 THEN BEGIN
          bp_t0=bp_t0[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,0:s[2]-1-FIX(ROUND(yshtv))]
          bp_t1=bp_t1[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,0:s[2]-1-FIX(ROUND(yshtv))]
          bt_t0=bt_t0[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,0:s[2]-1-FIX(ROUND(yshtv))]
          bt_t1=bt_t1[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,0:s[2]-1-FIX(ROUND(yshtv))]
          br_t0=br_t0[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,0:s[2]-1-FIX(ROUND(yshtv))]
          br_t1=br_t1[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,0:s[2]-1-FIX(ROUND(yshtv))]
        ENDIF ELSE BEGIN
          bp_t0=bp_t0[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          bp_t1=bp_t1[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          bt_t0=bt_t0[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          bt_t1=bt_t1[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          br_t0=br_t0[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          br_t1=br_t1[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
        ENDELSE
      ENDELSE
  
      br_t0[WHERE(FINITE(br_t0) EQ 0)]=0.
      bp_t0[WHERE(FINITE(bp_t0) EQ 0)]=0.
      bt_t0[WHERE(FINITE(bt_t0) EQ 0)]=0.
      br_t1[WHERE(FINITE(br_t1) EQ 0)]=0.
      bp_t1[WHERE(FINITE(bp_t1) EQ 0)]=0.
      bt_t1[WHERE(FINITE(bt_t1) EQ 0)]=0.
 
      s=SIZE(br_t0)
      delt=HMI_TIME_INTERVAL(t_obs_date1, t_obs_date0, /sec)
  
      ; calculate flow field
      windowsize=fwhm_dave4vm/delmm/0.725d
      dx=delmm*1e3
      dy=dx
      t_start=0d
      t_stop=delt*1d
  
      DO_DAVE4VM, bp_t0, bt_t0, br_t0, t_start, bp_t1, bt_t1, br_t1, t_stop, $
        dx, dy, vel4vm, magvm, windowsize=windowsize
  
      bx=magvm.bx
      by=magvm.by
      bz=magvm.bz
      
      IF N_TAGS(vel4vm) NE 0 THEN BEGIN
        ux0=vel4vm.u0
        uy0=vel4vm.v0
        uz0=vel4vm.w0
    
        B=SQRT(bx^2+by^2+bz^2)
        qx=bx/B
        qy=by/B
        qz=bz/B
    
        vB=qx*ux0+qy*uy0+qz*uz0
        ux=ux0-vB*qx
        uy=uy0-vB*qy
        uz=uz0-vB*qz
  
        ; calculate helicity and energy injection rates
        nx=s[1]
        ny=s[2]
        
        ux=ux*1e5
        uy=uy*1e5
        uz=uz*1e5
    
        px=delmm*1e8
    
        m=where(finite(ux) eq 1 and finite(uy) eq 1 and finite(uz) eq 1)
        fmask=dblarr(nx,ny)
        fmask[m]=1d
        m2=where(finite(ux) ne 1 and finite(uy) ne 1 and finite(uz) ne 1)
        ux[m2]=0d
        uy[m2]=0d
        uz[m2]=0d
        ux[0:10,*]=0d
        ux[nx-11:*,*]=0d
        ux[*,0:10]=0d
        ux[*,ny-11:*]=0d
        uy[0:10,*]=0d
        uy[nx-11:*,*]=0d
        uy[*,0:10]=0d
        uy[*,ny-11:*]=0d
        uz[0:10,*]=0d
        uz[nx-11:*,*]=0d
        uz[*,0:10]=0d
        uz[*,ny-11:*]=0d
    
        HELICITY_RATE_KO,bx,by,bz,ux,uy,uz,px,fmask,ga_inj,ga_shu,dhdt_inj,dhdt_shu,dhdt_a
        pos_dhdt_in=float(total(double(ga_inj>0d*fmask))*px*px/(10d^35))
        neg_dhdt_in=float(total(double(ga_inj<0d*fmask))*px*px/(10d^35))
        abs_neg_dhdt_in=abs(float(total(double(ga_inj<0d*fmask))*px*px/(10d^35)))
        abs_tot_dhdt_in=abs(pos_dhdt_in+neg_dhdt_in)
        tot_uns_dhdt_in=pos_dhdt_in+abs_neg_dhdt_in
  
        pos_dhdt_sh=float(total(double(ga_shu>0d*fmask))*px*px/(10d^35))
        neg_dhdt_sh=float(total(double(ga_shu<0d*fmask))*px*px/(10d^35))
        abs_neg_dhdt_sh=abs(float(total(double(ga_shu<0d*fmask))*px*px/(10d^35)))
        abs_tot_dhdt_sh=abs(pos_dhdt_sh+neg_dhdt_sh)
        tot_uns_dhdt_sh=pos_dhdt_sh+abs_neg_dhdt_sh
        
        abs_tot_dhdt=abs(pos_dhdt_in+neg_dhdt_in+pos_dhdt_sh+neg_dhdt_sh)
        abs_tot_dhdt_in_plus_sh=abs_tot_dhdt_in+abs_tot_dhdt_sh
        tot_uns_dhdt=pos_dhdt_in+abs_neg_dhdt_in+pos_dhdt_sh+abs_neg_dhdt_sh
  
        POYNTING_FLUX_CALC,px,bz,bx,by,ux,uy,uz=uz,shear_den,emerg_den,tot_den,tot_shear,tot_emerg,tot_flux,/cartesian
        pos_dedt_in=float(total(double(emerg_den>0d*fmask))*px*px/(10d^24))
        neg_dedt_in=float(total(double(emerg_den<0d*fmask))*px*px/(10d^24))
        abs_neg_dedt_in=abs(float(total(double(emerg_den<0d*fmask))*px*px/(10d^24)))
        abs_tot_dedt_in=abs(pos_dedt_in+neg_dedt_in)
        tot_uns_dedt_in=pos_dedt_in+abs_neg_dedt_in
        
        pos_dedt_sh=float(total(double(shear_den>0d*fmask))*px*px/(10d^24))
        neg_dedt_sh=float(total(double(shear_den<0d*fmask))*px*px/(10d^24))
        abs_neg_dedt_sh=abs(float(total(double(shear_den<0d*fmask))*px*px/(10d^24)))
        abs_tot_dedt_sh=abs(pos_dedt_sh+neg_dedt_sh)
        tot_uns_dedt_sh=pos_dedt_sh+abs_neg_dedt_sh
        
        abs_tot_dedt=abs(pos_dedt_in+neg_dedt_in+pos_dedt_sh+neg_dedt_sh)
        abs_tot_dedt_in_plus_sh=abs_tot_dedt_in+abs_tot_dedt_sh
        tot_uns_dedt=pos_dedt_in+abs_neg_dedt_in+pos_dedt_sh+abs_neg_dedt_sh
              
        output_helicity_energy={pos_dhdt_in:pos_dhdt_in, abs_neg_dhdt_in:abs_neg_dhdt_in, abs_tot_dhdt_in:abs_tot_dhdt_in, tot_uns_dhdt_in:tot_uns_dhdt_in, $
                                pos_dhdt_sh:pos_dhdt_sh, abs_neg_dhdt_sh:abs_neg_dhdt_sh, abs_tot_dhdt_sh:abs_tot_dhdt_sh, tot_uns_dhdt_sh:tot_uns_dhdt_sh, $
                                abs_tot_dhdt:abs_tot_dhdt, abs_tot_dhdt_in_plus_sh:abs_tot_dhdt_in_plus_sh, tot_uns_dhdt:tot_uns_dhdt, $ 
                                pos_dedt_in:pos_dedt_in, abs_neg_dedt_in:abs_neg_dedt_in, abs_tot_dedt_in:abs_tot_dedt_in, tot_uns_dedt_in:tot_uns_dedt_in, $
                                pos_dedt_sh:pos_dedt_sh, abs_neg_dedt_sh:abs_neg_dedt_sh, abs_tot_dedt_sh:abs_tot_dedt_sh, tot_uns_dedt_sh:tot_uns_dedt_sh, $
                                abs_tot_dedt:abs_tot_dedt, abs_tot_dedt_in_plus_sh:abs_tot_dedt_in_plus_sh, tot_uns_dedt:tot_uns_dedt}
      ENDIF ELSE BEGIN
        output_helicity_energy={pos_dhdt_in:0., abs_neg_dhdt_in:0., abs_tot_dhdt_in:0., tot_uns_dhdt_in:0., pos_dhdt_sh:0., abs_neg_dhdt_sh:0., abs_tot_dhdt_sh:0., tot_uns_dhdt_sh:0., $
                                abs_tot_dhdt:0., abs_tot_dhdt_in_plus_sh:0., tot_uns_dhdt:0., pos_dedt_in:0., abs_neg_dedt_in:0., abs_tot_dedt_in:0., tot_uns_dedt_in:0., pos_dedt_sh:0., $ 
                                abs_neg_dedt_sh:0., abs_tot_dedt_sh:0., tot_uns_dedt_sh:0., abs_tot_dedt:0., abs_tot_dedt_in_plus_sh:0., tot_uns_dedt:0.}    
      ENDELSE 
    ENDELSE
      
  ENDIF ELSE BEGIN
  
    t_obs_date0=anytim(round(idx_t0.t_obs-6.3072000E+7,/L64),/CCSDS)+'Z' 
    s0=SIZE(br_t0)
    br_t0=CONGRID(br_t0, s0[1]/binxy_helicity_energy*binxy_helicity_energy, s0[2]/binxy_helicity_energy*binxy_helicity_energy) ;bz
    br_t0=REBIN(br_t0, s0[1]/binxy_helicity_energy, s0[2]/binxy_helicity_energy)
    
    bt_t0=CONGRID(-1.*bt_t0, s0[1]/binxy_helicity_energy*binxy_helicity_energy, s0[2]/binxy_helicity_energy*binxy_helicity_energy) ;by
    bt_t0=REBIN(bt_t0, s0[1]/binxy_helicity_energy, s0[2]/binxy_helicity_energy)
    
    bp_t0=CONGRID(bp_t0, s0[1]/binxy_helicity_energy*binxy_helicity_energy, s0[2]/binxy_helicity_energy*binxy_helicity_energy) ;bx
    bp_t0=REBIN(bp_t0, s0[1]/binxy_helicity_energy, s0[2]/binxy_helicity_energy)
    
    t_obs_date1=anytim(round(idx_t1.t_obs-6.3072000E+7,/L64),/CCSDS)+'Z' 
    s1=SIZE(br_t1)
    br_t1=CONGRID(br_t1, s1[1]/binxy_helicity_energy*binxy_helicity_energy, s1[2]/binxy_helicity_energy*binxy_helicity_energy)
    br_t1=REBIN(br_t1, s1[1]/binxy_helicity_energy, s1[2]/binxy_helicity_energy)
    
    bt_t1=CONGRID(-1.*bt_t1, s1[1]/binxy_helicity_energy*binxy_helicity_energy, s1[2]/binxy_helicity_energy*binxy_helicity_energy)
    bt_t1=REBIN(bt_t1, s1[1]/binxy_helicity_energy, s1[2]/binxy_helicity_energy)
    
    bp_t1=CONGRID(bp_t1, s1[1]/binxy_helicity_energy*binxy_helicity_energy, s1[2]/binxy_helicity_energy*binxy_helicity_energy)
    bp_t1=REBIN(bp_t1, s1[1]/binxy_helicity_energy, s1[2]/binxy_helicity_energy)
    
    
    ; checking whether the two images have a common field-of-view or not
    IF (idx_t0.londtmax LT idx_t1.londtmin) THEN BEGIN
      output_helicity_energy={pos_dhdt_in:0., abs_neg_dhdt_in:0., abs_tot_dhdt_in:0., tot_uns_dhdt_in:0., pos_dhdt_sh:0., abs_neg_dhdt_sh:0., abs_tot_dhdt_sh:0., tot_uns_dhdt_sh:0., $
                              abs_tot_dhdt:0., abs_tot_dhdt_in_plus_sh:0., tot_uns_dhdt:0., pos_dedt_in:0., abs_neg_dedt_in:0., abs_tot_dedt_in:0., tot_uns_dedt_in:0., pos_dedt_sh:0., $ 
                              abs_neg_dedt_sh:0., abs_tot_dedt_sh:0., tot_uns_dedt_sh:0., abs_tot_dedt:0., abs_tot_dedt_in_plus_sh:0., tot_uns_dedt:0.}
    ENDIF ELSE BEGIN
    
      ; cut the sub-regions from the two images, which have the same x and y sizes
      IF (s0[1] le s1[1]) THEN BEGIN
        IF (s0[2] le s1[2]) THEN BEGIN
          bp_t0=bp_t0[0:s0[1]-1,0:s0[2]-1]
          bp_t1=bp_t1[(s1[1]-s0[1])/2:(s1[1]-1-ROUND((s1[1]-s0[1])/2.)),(s1[2]-s0[2])/2:(s1[2]-1-ROUND((s1[2]-s0[2])/2.))]
          bt_t0=bt_t0[0:s0[1]-1,0:s0[2]-1]
          bt_t1=bt_t1[(s1[1]-s0[1])/2:(s1[1]-1-ROUND((s1[1]-s0[1])/2.)),(s1[2]-s0[2])/2:(s1[2]-1-ROUND((s1[2]-s0[2])/2.))]
          br_t0=br_t0[0:s0[1]-1,0:s0[2]-1]
          br_t1=br_t1[(s1[1]-s0[1])/2:(s1[1]-1-ROUND((s1[1]-s0[1])/2.)),(s1[2]-s0[2])/2:(s1[2]-1-ROUND((s1[2]-s0[2])/2.))]
        ENDIF ELSE BEGIN
          bp_t0=bp_t0[0:s0[1]-1,(s0[2]-s1[2])/2:(s0[2]-1-ROUND((s0[2]-s1[2])/2.))]
          bp_t1=bp_t1[(s1[1]-s0[1])/2:(s1[1]-1-ROUND((s1[1]-s0[1])/2.)),0:s1[2]-1]
          bt_t0=bt_t0[0:s0[1]-1,(s0[2]-s1[2])/2:(s0[2]-1-ROUND((s0[2]-s1[2])/2.))]
          bt_t1=bt_t1[(s1[1]-s0[1])/2:(s1[1]-1-ROUND((s1[1]-s0[1])/2.)),0:s1[2]-1]
          br_t0=br_t0[0:s0[1]-1,(s0[2]-s1[2])/2:(s0[2]-1-ROUND((s0[2]-s1[2])/2.))]
          br_t1=br_t1[(s1[1]-s0[1])/2:(s1[1]-1-ROUND((s1[1]-s0[1])/2.)),0:s1[2]-1]
        ENDELSE
      ENDIF ELSE BEGIN
        IF (s0[2] le s1[2]) THEN BEGIN
          bp_t0=bp_t0[(s0[1]-s1[1])/2:(s0[1]-1-ROUND((s0[1]-s1[1])/2.)),0:s0[2]-1]
          bp_t1=bp_t1[0:s1[1]-1,(s1[2]-s0[2])/2:(s1[2]-1-ROUND((s1[2]-s0[2])/2.))]
          bt_t0=bt_t0[(s0[1]-s1[1])/2:(s0[1]-1-ROUND((s0[1]-s1[1])/2.)),0:s0[2]-1]
          bt_t1=bt_t1[0:s1[1]-1,(s1[2]-s0[2])/2:(s1[2]-1-ROUND((s1[2]-s0[2])/2.))]
          br_t0=br_t0[(s0[1]-s1[1])/2:(s0[1]-1-ROUND((s0[1]-s1[1])/2.)),0:s0[2]-1]
          br_t1=br_t1[0:s1[1]-1,(s1[2]-s0[2])/2:(s1[2]-1-ROUND((s1[2]-s0[2])/2.))]
        ENDIF ELSE BEGIN
          bp_t0=bp_t0[(s0[1]-s1[1])/2:(s0[1]-1-ROUND((s0[1]-s1[1])/2.)),(s0[2]-s1[2])/2:(s0[2]-1-ROUND((s0[2]-s1[2])/2.))]
          bp_t1=bp_t1[0:s1[1]-1,0:s1[2]-1]
          bt_t0=bt_t0[(s0[1]-s1[1])/2:(s0[1]-1-ROUND((s0[1]-s1[1])/2.)),(s0[2]-s1[2])/2:(s0[2]-1-ROUND((s0[2]-s1[2])/2.))]
          bt_t1=bt_t1[0:s1[1]-1,0:s1[2]-1]
          br_t0=br_t0[(s0[1]-s1[1])/2:(s0[1]-1-ROUND((s0[1]-s1[1])/2.)),(s0[2]-s1[2])/2:(s0[2]-1-ROUND((s0[2]-s1[2])/2.))]
          br_t1=br_t1[0:s1[1]-1,0:s1[2]-1]
        ENDELSE
      ENDELSE
    
      ; alignment using a correlation method
      s=SIZE(br_t0)
    
      sht=DAVE_MULTI(fwhm_align/delmm/0.725,  smooth(br_t0,ROUND(5./delmm),/EDGE_TRUNCATE,/NAN), smooth(br_t1,ROUND(5./delmm),/EDGE_TRUNCATE,/NAN))
      xsht=REFORM(sht[0,*,*],s[1],s[2])
      xshtv=MEAN(xsht[WHERE(ABS(br_t0) GT 500.)], /nan)
      ysht=REFORM(sht[1,*,*],s[1],s[2])
      yshtv=MEAN(ysht[WHERE(ABS(br_t0) GT 500.)], /nan)
      br_t1=SHIFT(br_t1,-1*FIX(ROUND(xshtv)),-1*FIX(ROUND(yshtv)))
      bp_t1=SHIFT(bp_t1,-1*FIX(ROUND(xshtv)),-1*FIX(ROUND(yshtv)))
      bt_t1=SHIFT(bt_t1,-1*FIX(ROUND(xshtv)),-1*FIX(ROUND(yshtv)))   
    
      ; cut the "co-alinged" sub-regions from the two images
      IF FIX(ROUND(xshtv)) ge 0 THEN BEGIN
        IF FIX(ROUND(yshtv)) ge 0 THEN BEGIN
          bp_t0=bp_t0[0:s[1]-1-FIX(ROUND(xshtv)),0:s[2]-1-FIX(ROUND(yshtv))]
          bp_t1=bp_t1[0:s[1]-1-FIX(ROUND(xshtv)),0:s[2]-1-FIX(ROUND(yshtv))]
          bt_t0=bt_t0[0:s[1]-1-FIX(ROUND(xshtv)),0:s[2]-1-FIX(ROUND(yshtv))]
          bt_t1=bt_t1[0:s[1]-1-FIX(ROUND(xshtv)),0:s[2]-1-FIX(ROUND(yshtv))]
          br_t0=br_t0[0:s[1]-1-FIX(ROUND(xshtv)),0:s[2]-1-FIX(ROUND(yshtv))]
          br_t1=br_t1[0:s[1]-1-FIX(ROUND(xshtv)),0:s[2]-1-FIX(ROUND(yshtv))]
        ENDIF ELSE BEGIN
          bp_t0=bp_t0[0:s[1]-1-FIX(ROUND(xshtv)),(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          bp_t1=bp_t1[0:s[1]-1-FIX(ROUND(xshtv)),(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          bt_t0=bt_t0[0:s[1]-1-FIX(ROUND(xshtv)),(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          bt_t1=bt_t1[0:s[1]-1-FIX(ROUND(xshtv)),(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          br_t0=br_t0[0:s[1]-1-FIX(ROUND(xshtv)),(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          br_t1=br_t1[0:s[1]-1-FIX(ROUND(xshtv)),(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
        ENDELSE
      ENDIF ELSE BEGIN
        IF FIX(ROUND(yshtv)) ge 0 THEN BEGIN
          bp_t0=bp_t0[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,0:s[2]-1-FIX(ROUND(yshtv))]
          bp_t1=bp_t1[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,0:s[2]-1-FIX(ROUND(yshtv))]
          bt_t0=bt_t0[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,0:s[2]-1-FIX(ROUND(yshtv))]
          bt_t1=bt_t1[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,0:s[2]-1-FIX(ROUND(yshtv))]
          br_t0=br_t0[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,0:s[2]-1-FIX(ROUND(yshtv))]
          br_t1=br_t1[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,0:s[2]-1-FIX(ROUND(yshtv))]
        ENDIF ELSE BEGIN
          bp_t0=bp_t0[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          bp_t1=bp_t1[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          bt_t0=bt_t0[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          bt_t1=bt_t1[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          br_t0=br_t0[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
          br_t1=br_t1[(-1*FIX(ROUND(xshtv)))>0:s[1]-1,(-1*FIX(ROUND(yshtv)))>0:s[2]-1]
        ENDELSE
      ENDELSE
      
      br_t0[WHERE(FINITE(br_t0) EQ 0)]=0.
      bp_t0[WHERE(FINITE(bp_t0) EQ 0)]=0.
      bt_t0[WHERE(FINITE(bt_t0) EQ 0)]=0.
      br_t1[WHERE(FINITE(br_t1) EQ 0)]=0.
      bp_t1[WHERE(FINITE(bp_t1) EQ 0)]=0.
      bt_t1[WHERE(FINITE(bt_t1) EQ 0)]=0.
      
      IF NOT ISVALID(mpil_info) OR NOT ISVALID(mpil_xy) THEN GET_MPIL_REGION, br_t1, mpil_info, mpil_xy, binxy=binxy_mpil

      IF FINITE(mpil_xy[0]) THEN BEGIN

        tmp = WHERE(FINITE(mpil_info.n) AND (mpil_info.typ LE 1), n_mpil)

        IF n_mpil GE 1 THEN BEGIN
              
          s=SIZE(br_t0)          
          delt=HMI_TIME_INTERVAL(t_obs_date1, t_obs_date0, /sec)
        
          ; calculate flow field
          windowsize=fwhm_dave4vm/delmm/0.725d    
          dx=delmm*1e3
          dy=dx
          t_start=0d
          t_stop=delt*1d
          
          DO_DAVE4VM, bp_t0, bt_t0, br_t0, t_start, bp_t1, bt_t1, br_t1, t_stop, $
            dx, dy, vel4vm, magvm, windowsize=windowsize
          
          bx=magvm.bx
          by=magvm.by
          bz=magvm.bz
          
          IF N_TAGS(vel4vm) NE 0 THEN BEGIN 
            ux0=vel4vm.u0
            uy0=vel4vm.v0
            uz0=vel4vm.w0
            
            B=SQRT(bx^2+by^2+bz^2)
            qx=bx/B
            qy=by/B
            qz=bz/B
            
            vB=qx*ux0+qy*uy0+qz*uz0
            ux=ux0-vB*qx
            uy=uy0-vB*qy
            uz=uz0-vB*qz
             
            ; helicity and energy          
            nx=s[1]
            ny=s[2]
            
            ux=ux*1e5
            uy=uy*1e5
            uz=uz*1e5
            
            px=delmm*1e8
            
            m=where(finite(ux) eq 1 and finite(uy) eq 1 and finite(uz) eq 1)
            fmask=dblarr(nx,ny)
            fmask[m]=1d
            m2=where(finite(ux) ne 1 and finite(uy) ne 1 and finite(uz) ne 1)
            ux[m2]=0d
            uy[m2]=0d
            uz[m2]=0d
            ux[0:10,*]=0d
            ux[nx-11:*,*]=0d
            ux[*,0:10]=0d
            ux[*,ny-11:*]=0d
            uy[0:10,*]=0d
            uy[nx-11:*,*]=0d
            uy[*,0:10]=0d
            uy[*,ny-11:*]=0d
            uz[0:10,*]=0d
            uz[nx-11:*,*]=0d
            uz[*,0:10]=0d
            uz[*,ny-11:*]=0d
            
            HELICITY_RATE_KO,bx,by,bz,ux,uy,uz,px,fmask,ga_inj,ga_shu,dhdt_inj,dhdt_shu,dhdt_a
        
            POYNTING_FLUX_CALC,px,bz,bx,by,ux,uy,uz=uz,shear_den,emerg_den,tot_den,tot_shear,tot_emerg,tot_flux,/cartesian
            
            mpil_region=fltarr(nx,ny)
            
            FOR l=0, n_mpil-1 DO BEGIN
              IF (mpil_info[l].typ LE 1) THEN BEGIN
                mpil_x = REFORM(mpil_xy[0, mpil_info[tmp[l]].offset : mpil_info[tmp[l]].offset + mpil_info[tmp[l]].n - 1])
                mpil_y = REFORM(mpil_xy[1, mpil_info[tmp[l]].offset : mpil_info[tmp[l]].offset + mpil_info[tmp[l]].n - 1])
                FOR k=0, n_elements(mpil_x)-1 DO BEGIN
                  IF mpil_x[k]-extendpix GE 0 AND mpil_y[k]-extendpix GE 0 AND mpil_x[k]+extendpix LE nx-1 AND mpil_y[k]+extendpix LE ny-1 THEN BEGIN
                    mpil_region[mpil_x[k]-extendpix:mpil_x[k]+extendpix, mpil_y[k]-extendpix:mpil_y[k]+extendpix] = 1
                  ENDIF
                ENDFOR
              ENDIF
            ENDFOR
  
            ss_mpil_extendpix=WHERE(mpil_region EQ 1)

            pos_dhdt_in=float(total(double(ga_inj[ss_mpil_extendpix]>0d*fmask))*px*px/(10d^35))
            neg_dhdt_in=float(total(double(ga_inj[ss_mpil_extendpix]<0d*fmask))*px*px/(10d^35))
            abs_neg_dhdt_in=abs(float(total(double(ga_inj[ss_mpil_extendpix]<0d*fmask))*px*px/(10d^35)))
            abs_tot_dhdt_in=abs(pos_dhdt_in+neg_dhdt_in)
            tot_uns_dhdt_in=pos_dhdt_in+abs_neg_dhdt_in

            pos_dhdt_sh=float(total(double(ga_shu[ss_mpil_extendpix]>0d*fmask))*px*px/(10d^35))
            neg_dhdt_sh=float(total(double(ga_shu[ss_mpil_extendpix]<0d*fmask))*px*px/(10d^35))
            abs_neg_dhdt_sh=abs(float(total(double(ga_shu[ss_mpil_extendpix]<0d*fmask))*px*px/(10d^35)))
            abs_tot_dhdt_sh=abs(pos_dhdt_sh+neg_dhdt_sh)
            tot_uns_dhdt_sh=pos_dhdt_sh+abs_neg_dhdt_sh

            abs_tot_dhdt=abs(pos_dhdt_in+neg_dhdt_in+pos_dhdt_sh+neg_dhdt_sh)
            abs_tot_dhdt_in_plus_sh=abs_tot_dhdt_in+abs_tot_dhdt_sh
            tot_uns_dhdt=pos_dhdt_in+abs_neg_dhdt_in+pos_dhdt_sh+abs_neg_dhdt_sh

            pos_dedt_in=float(total(double(emerg_den[ss_mpil_extendpix]>0d*fmask))*px*px/(10d^24))
            neg_dedt_in=float(total(double(emerg_den[ss_mpil_extendpix]<0d*fmask))*px*px/(10d^24))
            abs_neg_dedt_in=abs(float(total(double(emerg_den[ss_mpil_extendpix]<0d*fmask))*px*px/(10d^24)))
            abs_tot_dedt_in=abs(pos_dedt_in+neg_dedt_in)
            tot_uns_dedt_in=pos_dedt_in+abs_neg_dedt_in

            pos_dedt_sh=float(total(double(shear_den[ss_mpil_extendpix]>0d*fmask))*px*px/(10d^24))
            neg_dedt_sh=float(total(double(shear_den[ss_mpil_extendpix]<0d*fmask))*px*px/(10d^24))
            abs_neg_dedt_sh=abs(float(total(double(shear_den[ss_mpil_extendpix]<0d*fmask))*px*px/(10d^24)))
            abs_tot_dedt_sh=abs(pos_dedt_sh+neg_dedt_sh)
            tot_uns_dedt_sh=pos_dedt_sh+abs_neg_dedt_sh

            abs_tot_dedt=abs(pos_dedt_in+neg_dedt_in+pos_dedt_sh+neg_dedt_sh)
            abs_tot_dedt_in_plus_sh=abs_tot_dedt_in+abs_tot_dedt_sh
            tot_uns_dedt=pos_dedt_in+abs_neg_dedt_in+pos_dedt_sh+abs_neg_dedt_sh
                    
            output_helicity_energy={pos_dhdt_in:pos_dhdt_in, abs_neg_dhdt_in:abs_neg_dhdt_in, abs_tot_dhdt_in:abs_tot_dhdt_in, tot_uns_dhdt_in:tot_uns_dhdt_in, $
                                    pos_dhdt_sh:pos_dhdt_sh, abs_neg_dhdt_sh:abs_neg_dhdt_sh, abs_tot_dhdt_sh:abs_tot_dhdt_sh, tot_uns_dhdt_sh:tot_uns_dhdt_sh, $
                                    abs_tot_dhdt:abs_tot_dhdt, abs_tot_dhdt_in_plus_sh:abs_tot_dhdt_in_plus_sh, tot_uns_dhdt:tot_uns_dhdt, $
                                    pos_dedt_in:pos_dedt_in, abs_neg_dedt_in:abs_neg_dedt_in, abs_tot_dedt_in:abs_tot_dedt_in, tot_uns_dedt_in:tot_uns_dedt_in, $
                                    pos_dedt_sh:pos_dedt_sh, abs_neg_dedt_sh:abs_neg_dedt_sh, abs_tot_dedt_sh:abs_tot_dedt_sh, tot_uns_dedt_sh:tot_uns_dedt_sh, $
                                    abs_tot_dedt:abs_tot_dedt, abs_tot_dedt_in_plus_sh:abs_tot_dedt_in_plus_sh, tot_uns_dedt:tot_uns_dedt}
     
          ENDIF ELSE BEGIN
            output_helicity_energy={pos_dhdt_in:0., abs_neg_dhdt_in:0., abs_tot_dhdt_in:0., tot_uns_dhdt_in:0., pos_dhdt_sh:0., abs_neg_dhdt_sh:0., abs_tot_dhdt_sh:0., tot_uns_dhdt_sh:0., $
                                    abs_tot_dhdt:0., abs_tot_dhdt_in_plus_sh:0., tot_uns_dhdt:0., pos_dedt_in:0., abs_neg_dedt_in:0., abs_tot_dedt_in:0., tot_uns_dedt_in:0., pos_dedt_sh:0., $ 
                                    abs_neg_dedt_sh:0., abs_tot_dedt_sh:0., tot_uns_dedt_sh:0., abs_tot_dedt:0., abs_tot_dedt_in_plus_sh:0., tot_uns_dedt:0.}
          ENDELSE
                  
        ENDIF ELSE BEGIN
          output_helicity_energy={pos_dhdt_in:0., abs_neg_dhdt_in:0., abs_tot_dhdt_in:0., tot_uns_dhdt_in:0., pos_dhdt_sh:0., abs_neg_dhdt_sh:0., abs_tot_dhdt_sh:0., tot_uns_dhdt_sh:0., $
                                  abs_tot_dhdt:0., abs_tot_dhdt_in_plus_sh:0., tot_uns_dhdt:0., pos_dedt_in:0., abs_neg_dedt_in:0., abs_tot_dedt_in:0., tot_uns_dedt_in:0., pos_dedt_sh:0., $ 
                                  abs_neg_dedt_sh:0., abs_tot_dedt_sh:0., tot_uns_dedt_sh:0., abs_tot_dedt:0., abs_tot_dedt_in_plus_sh:0., tot_uns_dedt:0.}
        ENDELSE
          
      ENDIF ELSE BEGIN
          output_helicity_energy={pos_dhdt_in:0., abs_neg_dhdt_in:0., abs_tot_dhdt_in:0., tot_uns_dhdt_in:0., pos_dhdt_sh:0., abs_neg_dhdt_sh:0., abs_tot_dhdt_sh:0., tot_uns_dhdt_sh:0., $
                                  abs_tot_dhdt:0., abs_tot_dhdt_in_plus_sh:0., tot_uns_dhdt:0., pos_dedt_in:0., abs_neg_dedt_in:0., abs_tot_dedt_in:0., tot_uns_dedt_in:0., pos_dedt_sh:0., $ 
                                  abs_neg_dedt_sh:0., abs_tot_dedt_sh:0., tot_uns_dedt_sh:0., abs_tot_dedt:0., abs_tot_dedt_in_plus_sh:0., tot_uns_dedt:0.}
      ENDELSE
  
    ENDELSE        
  
  ENDELSE

ENDIF ELSE BEGIN

  output_helicity_energy={pos_dhdt_in:0., abs_neg_dhdt_in:0., abs_tot_dhdt_in:0., tot_uns_dhdt_in:0., pos_dhdt_sh:0., abs_neg_dhdt_sh:0., abs_tot_dhdt_sh:0., tot_uns_dhdt_sh:0., $
                          abs_tot_dhdt:0., abs_tot_dhdt_in_plus_sh:0., tot_uns_dhdt:0., pos_dedt_in:0., abs_neg_dedt_in:0., abs_tot_dedt_in:0., tot_uns_dedt_in:0., pos_dedt_sh:0., $
                          abs_neg_dedt_sh:0., abs_tot_dedt_sh:0., tot_uns_dedt_sh:0., abs_tot_dedt:0., tot_uns_dedt:0.}

ENDELSE

RETURN, output_helicity_energy 
 
END